rainbowcrack (1.8-0kali2) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit,
    Repository, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.
  * Update standards version to 4.6.2, no changes needed.

  [ Ben Wilson ]
  * Update email address
  * New helper-script format
  * Remove template comment and switch spaces to tabs

  [ Steev Klimaszewski ]
  * Remove invalid upstream metadata
  * ci: Disable unsupported architectures.
  * ci: Disable non amd64 architecture builds.
  * d/watch: Add fake upstream tracking as there are unlikely
    to be updates any time in the future.
  * Add lintian overrides.

 -- Steev Klimaszewski <steev@kali.org>  Fri, 20 Oct 2023 12:35:19 -0500

rainbowcrack (1.8-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

  [ Sophie Brun ]
  * New upstream version 1.8
  * Use debhelper-compat 13
  * Bump Standards-Version to 4.5.0 (use https...)

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 09 Sep 2020 17:41:07 +0200

rainbowcrack (1.7-0kali2) kali-dev; urgency=medium

  * Update debian/control to build package only for amd64

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 12 Apr 2017 16:08:32 +0200

rainbowcrack (1.7-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Update installation: upstream provides now only binaries for amd64
  * Add a helper-script for rtmerge

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 12 Apr 2017 15:24:09 +0200

rainbowcrack (1.6.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Jul 2015 16:36:07 +0200

rainbowcrack (1.5-1kali0) kali; urgency=low

  * Initial release

 -- Devon Kearns <dookie@kali.org>  Mon, 21 Jan 2013 08:09:11 -0700
